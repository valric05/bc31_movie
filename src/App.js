import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Page/LoginPage/LoginPage";
import HomePage from "./Page/HomePage/HomePage";
import Layout from "./HOC/Layout";
import DetailPage from "./Page/DetailPage/DetailPage";
import SignUpPage from "./Page/SignUpPage/SignUpPage";
// import SpinnerComponent from "./Component/SpinnerComponent/SpinnerComponent";

function App() {
  return (
    <div className="">
      {/* <SpinnerComponent /> */}
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />

          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />

          <Route path="/login" element={<LoginPage />} />
          <Route path="/signup" element={<SignUpPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

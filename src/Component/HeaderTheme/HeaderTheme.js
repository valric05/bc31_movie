import React from "react";
import UserNav from "./UserNav";
import logo from "../../assets/logo.png";

export default function HeaderTheme() {
  let handleMenuButton = () => {
    const isExpanded = JSON.parse(
      document.querySelector("#menu").getAttribute("aria-expanded")
    );
    document.querySelector("#menu").setAttribute("aria-expanded", !isExpanded);
    document.querySelector('[role="menubar"]').classList.toggle("hidden");
    document.querySelector('[role="menubar"]').classList.toggle("flex");
    // document.querySelector("#homepage").classList.toggle("opacity-50");
  };

  return (
    <nav className="h-20 w-full px-10 flex items-center shadow-lg justify-between bg-white bg-opacity-95 fixed top-0 z-50 ">
      <img alt="logo" src={logo} style={{ height: "100%" }} className="h-20" />

      <button
        id="menu"
        class="
        lg:hidden
        hover:text-neutral-600
        transition-colors
        "
        aria-expanded="false"
        onClick={handleMenuButton}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke-width="1.5"
          stroke="currentColor"
          class="w-8 h-8"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
          />
        </svg>
      </button>

      <div
        role="menubar"
        className="
      z-40
      hidden
      flex-col
      gap-5
      absolute
      right-0
      top-16
      bg-white
      bg-opacity-95
      h-screen
      lg:flex
      lg:flex-row
      lg:justify-between
      lg:h-20
      lg:relative
      lg:top-0
      lg:w-4/6
      "
      >
        <a
          href="#list-movie"
          className="text-lg font-medium px-7 py-5 hover:text-red-500 hover:bg-neutral-100 "
        >
          Lịch Chiếu
        </a>
        <a
          href="#tab-movie"
          className="text-lg font-medium px-7 py-5 hover:text-red-500 hover:bg-neutral-100"
        >
          Cụm Rạp
        </a>
        <a
          href="#carousel-movie"
          className="text-lg font-medium px-7 py-5 hover:text-red-500 hover:bg-neutral-100"
        >
          Tin Tức
        </a>
        <a
          href=""
          className="text-lg font-medium px-7 py-5 hover:text-red-500 hover:bg-neutral-100"
        >
          Ứng Dụng
        </a>
        <div>
          <UserNav />
        </div>
      </div>
    </nav>
  );
}

import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../redux/actions/userAction";
import { localStorageServ } from "../../Service/localStorageService";
import { UserOutlined, ExportOutlined } from "@ant-design/icons";

export default function UserNav() {
  let dispatch = useDispatch();
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  const renderContent = () => {
    if (userInfor) {
      return (
        <div
          className="
          flex
          flex-col 
          text-lg 
          font-medium 
          text-neutral-500 
          px-10 
          mt-10 
          lg:p-0
          lg:m-0
          lg:flex-row
          lg:items-center"
        >
          <div className="lg:my-5">
            <UserOutlined className="border-2  p-2 border-neutral-500 rounded-full mx-3" />
            <span className="">{userInfor.hoTen}</span>
          </div>
          <div className="lg:m-5 lg:border-l-4 lg:-translate-y-1 border-neutral-400">
            <button
              onClick={handleLogout}
              className=" px-3 hover:text-red-500 transition-colors"
            >
              Đăng xuất
              <ExportOutlined className="ml-2 -translate-y-1 " />
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div
          className="
        flex
        flex-col 
        text-lg 
        font-medium 
        text-neutral-500  
        mt-10 
        px-5
        lg:p-0
        lg:m-0
        lg:flex-row
        lg:items-center
        "
        >
          <div>
            <button
              onClick={() => {
                window.location.href = "/login";
              }}
              className="lg:pt-5 items-center border-neutral-400 px-5 hover:text-red-500 transition-colors"
            >
              <ExportOutlined className="-translate-y-1 mr-5" />
              Đăng nhập
            </button>
          </div>
          <div className="hidden lg:flex">
            <button
              onClick={() => {
                window.location.href = "/sigup";
              }}
              className="lg:pt-5 border-neutral-400 px-5 hover:text-red-500 transition-colors"
            >
              Đăng ký
            </button>
          </div>
        </div>
      );
    }
  };

  const handleLogout = () => {
    localStorageServ.user.remove();
    dispatch(loginAction(null));
  };

  return <>{renderContent()}</>;
}

import React from "react";
import { Button, Form, Input, InputNumber, message } from "antd";
import LoginAnimate from "../LoginPage/LoginAnimate";
import { useDispatch } from "react-redux";
import { userService } from "../../Service/user.service";
import { sigupAction } from "../../redux/actions/userAction";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

export default function SignUpPage() {
  let dispatch = useDispatch();

  const onFinish = (values) => {
    userService
      .postSigup(values)
      .then((res) => {
        message.success("Đăng ký thành công");
        console.log(res);

        dispatch(sigupAction(res.data.content));
        window.location.href = "/login";

        // setTimeout(() => {
        //   history("/");
        // }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <div className="bg-pink-600 h-screen w-screen p-20">
        <div className=" font-extrabold text-transparent text-9xl bg-clip-text bg-gradient-to-r from-blue-900 to-yellow-600 font-mono ">
          RẠP PHIM TẠI GIA
        </div>

        <div className="container rounded-xl bg-pink-300 flex p-20">
          <div className="w-2/5 h-50 -translate-y-10 bg-cover">
            <LoginAnimate />
          </div>

          <div className="w-1/5"></div>
          <div className="w-3/5 p-20 bg-pink-100 rounded-xl">
            <Form
              {...layout}
              name="nest-messages"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name="hoTen"
                label="Họ tên"
                rules={[{ required: true, message: "Please input your name!" }]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="soDT"
                label="Số điện thoại"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="email"
                label="Email"
                rules={[
                  { type: "email" },
                  { required: true, message: "Please input your email !" },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="maNhom"
                label="Mã nhóm"
                rules={[
                  {
                    type: "number",
                    min: 0,
                    max: 10,
                    required: true,
                    message: "Please input your group !",
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item
                name="taiKhoan"
                label="Tài khoản"
                rules={[
                  { required: true, message: "Please input your username !" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="matKhau"
                label="Mật khẩu"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <button className="rounded px-5 py-2 text-white bg-red-500 text-lg">
                  ĐĂNG KÝ
                </button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}

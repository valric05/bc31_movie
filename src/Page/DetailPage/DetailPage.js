import { Progress } from "antd";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../Service/movie.service";

export default function DetailPage() {
  let { id } = useParams();
  const [movie, setMovie] = useState({});

  console.log("movie: ", movie);

  useEffect(() => {
    movieService
      .getMovieDetail(id)
      .then((res) => {
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="  bg-gradient-to-r from-emerald-900 to-rose-900">
      <div className="container mx-auto flex justify-center items-center py-10 space-x-10">
        <img src={movie.hinhAnh} className="w-60" alt="" />
        <div className="w-2/5">
          <p className="text-3xl font-medium text-red-500">{movie.tenPhim}</p>
          <p>{movie.moTa}</p>
          <button className="w-full bg-red-600 text-white rounded text-xl font-medium py-3 mt-5 hover:bg-red-700">
            Mua vé
          </button>
        </div>

        <Progress
          label="Danh gia"
          type="circle"
          strokeColor={{
            "0%": "#108ee9",
            "100%": "#87d068",
          }}
          percent={movie.danhGia * 10}
          format={(number) => {
            return (
              <span className="text-white font-medium">{number / 10} điểm</span>
            );
          }}
        />
      </div>
    </div>
  );
}

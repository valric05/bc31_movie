import React from "react";
import { Carousel } from "antd";
import { useEffect, useState } from "react";
import { movieService } from "../../Service/movie.service";

export default function CarouselMovie() {
  let [bannerList, setBannerList] = useState([]);

  useEffect(() => {
    movieService
      .getBannerMovies()
      .then((res) => {
        setBannerList(res.data.content);
        // console.log("bannerList: ", bannerList);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div id="carousel-movie" className="bg-neutral-200">
      {/* <p className="text-5xl font-medium tracking-[.9em] italic text-center drop-shadow-2xl text-rose-200 mb-10 ">
        PHIM ĐANG HOT
      </p> */}
      <Carousel autoplay>
        {bannerList.map((movie) => {
          return (
            <div className="w-screen pt-20">
              <img className="object-cover w-full" src={movie.hinhAnh} />
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}

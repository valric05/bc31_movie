import React, { useEffect, useState } from "react";
import { movieService } from "../../Service/movie.service";
import { Tabs } from "antd";
import ItemTabMovie from "./ItemTabMovie";
const { TabPane } = Tabs;

export default function TabMovie() {
  const [dataMovie, setDataMovie] = useState([]);

  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const onChange = (key) => {
    console.log(key);
  };

  const renderContent = () => {
    // Render logo he thong rap
    return dataMovie.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-20" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
          >
            {/* Render ten cua cum rap */}
            {heThongRap.lstCumRap.map((cumRap, key) => {
              return (
                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="shadow"
                  >
                    {/* Render phim trong rap */}
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie phim={phim} />;
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  const renderTenCumRap = (cumRap) => {
    return (
      <div className="text-left w-60 ">
        <p className="text-green-700 truncate">{cumRap.tenCumRap}</p>
        <p className=" text-black truncate">{cumRap.diaChi}</p>
        <button className="text-red-600">[ Xem chi tiết ] </button>
      </div>
    );
  };

  return (
    <div
      id="tab-movie"
      className="container flex justify-center md:w-full mx-auto my-20"
    >
      <Tabs
        style={{ height: 500 }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
      >
        {renderContent()}
      </Tabs>
    </div>
  );
}

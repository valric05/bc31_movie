import { Card } from "antd";
import Meta from "antd/lib/card/Meta";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getMovieListActionService } from "../../redux/actions/movieAction";
import { MOVIE_DESCRIPTION_LENGTH } from "../../redux/constant/movieConstant";

export default function ListMovie() {
  let dispatch = useDispatch();

  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });

  useEffect(() => {
    dispatch(getMovieListActionService());
  }, []);

  let renderMovieList = () => {
    return movieList
      .filter((item) => {
        return item.hinhAnh != null;
      })
      .map((item, key) => {
        return (
          <Card
            key={item.maPhim}
            hoverable
            style={{
              width: 250,
            }}
            className="shadow-gray-700 hover:shadow-xl hover:shadow-slate-800 rounded-lg"
            cover={
              <div style={{ overflow: "hidden", height: "350px" }}>
                <img
                  alt="example"
                  style={{ height: "100%", width: "100%" }}
                  src={item.hinhAnh}
                  className="rounded-sm"
                />
              </div>
            }
          >
            <Meta
              title={
                <>
                  <span className="px-2 mr-2 bg-red-600 text-white rounded">
                    C18
                  </span>
                  <span className="text-black text-xl">{item.tenPhim}</span>
                </>
              }
              description={
                <p className="font-bold">
                  {item.moTa.length > MOVIE_DESCRIPTION_LENGTH
                    ? item.moTa.slice(0, MOVIE_DESCRIPTION_LENGTH - 1) + "..."
                    : item.moTa}
                </p>
              }
            />
            <NavLink to={`detail/${item.maPhim}`}>
              <button className="w-full bg-red-600 text-white rounded text-xl font-medium py-3 mt-5 hover:bg-red-700">
                Mua vé
              </button>
            </NavLink>
          </Card>
        );
      });
  };

  return (
    <div className="container flex justify-center  md:w-full mx-auto my-20 shadow-md">
      <div
        id="list-movie"
        className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 gap-4 auto-rows-max"
      >
        {renderMovieList()}
      </div>
    </div>
  );
}

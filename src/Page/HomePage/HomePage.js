import React from "react";
import FooterTheme from "../../Component/FooterTheme/FooterTheme";
import CarouselMovie from "./CarouselMovie";
import ListMovie from "./ListMovie";
import TabMovie from "./TabMovie";

export default function HomePage() {
  return (
    <div id="homePage" className="bg-white scroll-smooth">
      <CarouselMovie />
      <ListMovie />
      <TabMovie />
      <FooterTheme />
    </div>
  );
}

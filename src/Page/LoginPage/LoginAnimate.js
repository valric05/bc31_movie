import React from "react";
import Lottie from "lottie-react";
import bgAnimate from "../../assets/loginAnimate.json";

export default function LoginAnimate() {
  return (
    <div className="-translate-y-50">
      <Lottie animationData={bgAnimate} height={300} />
    </div>
  );
}

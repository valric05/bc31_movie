import React from "react";
import { Form, Input, message } from "antd";
import { userService } from "../../Service/user.service";
import { localStorageServ } from "../../Service/localStorageService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginAction } from "../../redux/actions/userAction";
import LoginAnimate from "./LoginAnimate";

export default function LoginPage() {
  let dispatch = useDispatch();

  const onFinish = (values) => {
    userService
      .postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        console.log(res);

        dispatch(loginAction(res.data.content));
        localStorageServ.user.set(res.data.content);
        window.location.href = "/";

        // setTimeout(() => {
        //   history("/");
        // }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg-pink-600 h-screen w-screen p-20">
      <div className=" font-extrabold text-transparent text-9xl bg-clip-text bg-gradient-to-r from-blue-900 to-yellow-600 font-mono ">
        RẠP PHIM TẠI GIA
      </div>
      <div className="container rounded-xl bg-pink-300 flex p-20">
        <div className="w-2/5 h-50 -translate-y-10 bg-cover">
          <LoginAnimate />
        </div>
        <div className="w-1/5"></div>
        <div className="w-3/5 p-20 bg-pink-100 rounded-xl">
          <Form
            name="basic"
            layout="vertical"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Tài Khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input placeholder="CuongDepTrai" />
            </Form.Item>

            <Form.Item
              label="Mật Khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password placeholder="anhCuong123" />
            </Form.Item>

            <div className="flex justify-center">
              <button className="rounded px-5 py-2 text-white bg-red-500 text-lg">
                ĐĂNG NHẬP
              </button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}

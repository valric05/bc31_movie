import React from "react";
import HeaderTheme from "../Component/HeaderTheme/HeaderTheme";

export default function Layout({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
    </div>
  );
}

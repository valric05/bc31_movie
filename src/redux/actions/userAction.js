export let loginAction = (dataLogin) => {
  return {
    type: "LOGIN",
    payload: dataLogin,
  };
};

export let sigupAction = (dataSignUp) => {
  return {
    type: "SIGNUP",
    payload: dataSignUp,
  };
};

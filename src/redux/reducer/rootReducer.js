import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { movieReducer } from "./movieReducer.js";
// import { spinnerReducer } from "./spinnerReducer";

export let rootReducer = combineReducers({
  userReducer,
  movieReducer,
  //   spinnerReducer,
});
